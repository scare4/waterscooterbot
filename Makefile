BUILD = gcc core/*.c -o bot -pthread -Wall -O3 -Llua/include -Ilua/include -llua -lm -ldl -Wl,-E 

default: bot

test: BUILD += -D__DEBUG
test: bot

bot: core/*.c
	$(BUILD)

clean:
	rm bot