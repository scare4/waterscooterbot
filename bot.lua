    -- waterscooterbot
    -- Copyright (C) 2019  Madeline MARIN-PACHE

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU General Public License as published by
    -- the Free Software Foundation, either version 3 of the License, or
    -- (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    -- GNU General Public License for more details.

    -- You should have received a copy of the GNU General Public License
    -- along with this program.  If not, see <http://www.gnu.org/licenses/>.



print("starting bot init")
--loading libs
lfs = require("lfs")
print("loaded lfs")
json = require("json")
print("libs loaded")

--loading bot data
bot = {}
bot.packages = {}
bot.commands = {}
bot.data = {}
bot.harddata = {}
bot.lib = {}
bot.config = require("config")
bot.stop = false
bot.customFilters = {}
local config = bot.config
local started = false

print("loaded confing and setup global values")

function bot.chat(msg, channel)
    __queueMessage(channel, msg)
end

print("checking folders")
--checking folders
local tst = lfs.attributes("packages")
if not tst then
    lfs.mkdir("packages")
end

local tst = lfs.attributes("data")
if not tst then
    lfs.mkdir("data")
end
tst = nil

for k, v in pairs(bot.harddata) do
    local o = io.open("data/"..k..".json", "r")
    if o ~= nil then
        bot.harddata[k] = json.decode(o:read("*all"))
    end
    o:close()
end
print("validated folders")

--package utils
function bot.loadPackages()
    for dir in lfs.dir("packages/") do
        if dir ~= '.' and dir ~= '..' then
            bot.packloc = dir
            bot.loadPackage()
            --pcall(bot.loadPackage)
        end
    end
end

function bot.loadPackage()
    location = bot.packloc
    --require package file
    local packf = dofile("packages/"..location.."/package.lua")

    --check package file
    if packf and packf.id and packf.desc then
        local pack = {}

        if packf.commands then
            pack.commands = {}

            --list commands
            for k, v in pairs(packf.commands) do
                if pack.commands[k] then
                    error("command "..k.." has a duplicate in the package")
                end
                pack.commands[k] = dofile("packages/"..location.."/"..v..".lua")
            end
        end

        if packf.init then
            packf.init()
        end

        if type(packf.perm) ~= "number" and type(packf.perm) ~= "table" then
            packf.perm = 0
        end

        --register package
        if bot.packages[packf.id] then
            error("package "..packf.id.." already registered")
        end
        bot.packages[packf.id] = {
            id = packf.id,
            desc = packf.desc,
            loc = location
        }

        if packf.customFilters then
            bot.packages[packf.id].customFilters = {}

            for k, v in pairs(packf.customFilters) do
                if bot.customFilters[k] then
                    error("custom filter "..k.. " is already registered")
                end
                bot.customFilters[k] = dofile("packages/"..location.."/"..v..".lua")
                table.insert(bot.packages[packf.id].customFilters, k)
                print("custom filter "..k.." for command "..v.." loaded successfully")
            end
        end

        if pack.commands then
            bot.packages[packf.id].commands = {}
            --register commands
            for k, v in pairs(pack.commands) do
                if bot.commands[k] then
                    error("command "..k.." is already registered")
                end
                bot.commands[k] = {
                    package = bot.packages[packf.id],
                    id = k,
                    command = v,
                    perm = packf.perm
                }
                if type(packf.perm) == "table" then
                    if packf.perm[k] ~= nil then
                        bot.commands[k].perm = packf.perm[k]
                    else
                        bot.commands[k].perm = 0
                    end
                end
                bot.packages[packf.id].commands[k] = bot.commands[k]
                print("command "..k.." loaded with perm "..tostring(bot.commands[k].perm))
            end
        end
        print("package "..packf.id.." loaded")
    else
        error("folder "..location.." does not contain a valid package file")
    end
end

function bot.reloadPackage()
    local package = bot.packloc
    for k, v in pairs(bot.packages) do
        if v.id == package then
            bot.packloc = v.loc
            bot.unloadPackage(v)
            bot.loadPackage(bot.packloc)
            return
        end
    end
    error("package "..package.." does not exist")
end

function bot.unloadPackage()
    local package = bot.packloc
    local packId = ""
    if type(package) == "string" then
        for k, v in pairs(bot.packages) do
            if v.id == package then
                package = v
                packId = k
                break
            end
        end
    end
    if packId == "" then
        error("package not found")
    end
    if package.commands then
        for k, v in pairs(package.commands) do
            bot.commands[k].command = nil
            bot.commands[k].id = nil
            bot.commands[k] = nil
            package.commands[k] = nil
        end
        bot.packages[package.id].commands = nil
    end
    if package.customFilters then
        for k, v in ipairs(package.customFilters) do
            bot.customFilters[v] = nil
            package.customFilters[v] = nil
        end
        bot.packages[package.id].customFilters = nil
    end
    bot.packages[package.id] = nil
    package = nil
end

print("loading packages")
bot.packloc = ""
bot.loadPackages()
bot.packloc = nil
print("packages loaded")

function bot.split(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t = {}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        table.insert(t, str)
    end
    return t
end

function bot.formulateSec(secs)
    if type(secs) ~= "number" then
        return {hours = -1, minutes = -1, seconds = -1}
    end
    local r = {}
    r.hours = math.floor(secs / 3600)
    secs = secs - r.hours * 3600
    r.minutes = math.floor(secs / 60)
    secs = secs - r.minutes * 60
    r.seconds = secs
    return r
end

-- function bot.tempDisconnect(secs)
--     started = false
--     bot.client:close()
--     print("disconnecting for "..tostring(secs))
--     os.execute("sleep "..tostring(secs))

--     bot.client:connect(bot.Server, bot.Port)
--     bot.client:send("PASS "..bot.Pass)
--     bot.client:send("NICK "..bot.Name)
--     bot.client:send("JOIN "..bot.Channel)
-- end

--message utils
function parseMsg(msg)
    -- if msg:sub(-18) == "End of /NAMES list" then
    --     started = true
    --     --print("ready")
    --     return msg
    -- end
    -- if not started then
    --     return msg
    -- end
    --print ("parsing", msg)
    local message = {}
    local i = 1
    if msg:sub(1, 4) == "PING" then
        pong()
        return
    end
    if msg:find("PRIVMSG") == nil then
        return
    end
    while true do
        if msg:sub(i, i) == '!' then
            message.user = msg:sub(2, i - 1)
            break
        end
        i = i + 1
    end
    while true do
        if msg:sub(i, i) == '#' then
            break
        end
        i = i + 1
    end
    local j = i
    while true do
        if msg:sub(i, i) == ' ' then
            message.channel = msg:sub(j, i - 1)
            break
        end
        i = i + 1
    end
    message.msg = msg:sub(i + 2)
    if message.msg and message.msg:sub(1, 1) == config.prefix then
        bot.runCommand(message)
    else
        local lmsg = string.lower(message.msg)
        for filter, command in pairs(bot.customFilters) do
            --print("'"..lmsg.."'")
            --print(string.find(lmsg, string.lower(filter)))
            if string.find(lmsg, string.lower(filter)) then
                --print("boop")
                bot.runFilter(filter, message)
            end
        end
    end
    --return message
end

function bot.getPerm(user)
    if user == "scarelol444" then
        return 9999
    end
    if user == "voxf0x" then
        return 150
    end
    if user == "shalooonsh" or user == "prezkain" or user == "tardismechanic" or user == "kayllair67" then
        return 100
    end
    return 0
end

function bot.runCommand(command)
    for k, v in pairs(bot.harddata) do
        local o = io.open("data/"..k..".json", "r")
        if o ~= nil then
            bot.harddata[k] = json.decode(o:read("*all"))
            o:close()
        end
    end
    local args = bot.split(command.msg:sub(#config.prefix + 1))
    if bot.commands[args[1]] then
        if bot.getPerm(command.user) >= bot.commands[args[1]].perm then
            bot.commands[args[1]].command(args, command.user, command.channel, command.msg)
        else
            bot.chat("@"..command.user.." you do not have permission to use that command", command.channel)
        end
    end
    for k, v in pairs(bot.harddata) do
        local o = io.open("data/"..k..".json", "w")
        o:write(json.encode(v))
        o:close()
    end
end

function bot.runFilter(filter, msg)
    for k, v in pairs(bot.harddata) do
        local o = io.open("data/"..k..".json", "r")
        if o ~= nil then
            bot.harddata[k] = json.decode(o:read("*all"))
            o:close()
        end
    end
    local args = bot.split(msg.msg)
    bot.customFilters[filter](args, msg.user, msg.channel, msg.msg)
    for k, v in pairs(bot.harddata) do
        local o = io.open("data/"..k..".json", "w")
        o:write(json.encode(v))
        o:close()
    end
end

print("finished bot init")
