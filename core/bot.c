/*
    waterscooterbot
    Copyright (C) 2019  Madeline MARIN-PACHE

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "bot.h"

//ready status for send/receive
unsigned short recvRd = 0, sendRd = 0;

pthread_mutex_t recvRdLock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t sendRdLock = PTHREAD_MUTEX_INITIALIZER;

void readyReceive() {
    pthread_mutex_lock(&recvRdLock);
    recvRd = ~recvRd;
    pthread_mutex_unlock(&recvRdLock);
}

void readySend() {
    pthread_mutex_lock(&sendRdLock);
    sendRd = ~sendRd;
    pthread_mutex_unlock(&sendRdLock);
}

void __sleep(unsigned int n) {
    struct timespec t;
    t.tv_nsec = 0;
    t.tv_sec = n;
    nanosleep(&t, NULL);
}

void __sleepn(unsigned int n) {
    struct timespec t;
    t.tv_nsec = n;
    t.tv_sec = 0;
    nanosleep(&t, NULL);
}

void printlog(FILE* f, char const* fmt, ...){
    va_list ap;
    va_start(ap, fmt);
    vprintf(fmt, ap);
    va_end(ap);
    va_start(ap, fmt);
    vfprintf(f, fmt, ap);
    va_end(ap);
    fflush(f);
}

int main(int argc, char** argv) {
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int sfd, s;

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;
    hints.ai_protocol = 0;

    s = getaddrinfo("irc.chat.twitch.tv", "6667", &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        exit(EXIT_FAILURE);
    }

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sfd == -1)
            continue;

        if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1)
            break;

        close(sfd);
    }
    
    if (rp == NULL) {
        fprintf(stderr, "Could not connect\n");
        exit(EXIT_FAILURE);
    }
    printf("socket: %d\n", sfd);
    freeaddrinfo(result);
    char* tmpbuf = calloc(44, sizeof(char));
    int cc = snprintf(tmpbuf, 44, "PASS %s\r\n", TOKEN) + 1;
    if (send(sfd, tmpbuf, cc, 0) != cc) {
        fprintf(stderr, "partial/failed write pass\n");
        exit(EXIT_FAILURE);
    }
    cc = snprintf(tmpbuf, 44, "NICK %s\r\n", NICK) + 1;
    if (send(sfd, tmpbuf, cc, 0) != cc) {
        fprintf(stderr, "partial/failed write nick\n");
        exit(EXIT_FAILURE);
    }
    cc = snprintf(tmpbuf, 44, "JOIN %s\r\n", CHANNEL) + 1;
    if (send(sfd, tmpbuf, cc, 0) != cc) {
        fprintf(stderr, "partial/failed write join\n");
        exit(EXIT_FAILURE);
    }

    threadData tdat;
    tdat.sfd = sfd;
    tdat.logFile = fopen("bot.log", "w");

    pthread_t receiver_thread;
    if(pthread_create(&receiver_thread, NULL, init_receiver, &tdat)) {
        perror("unable to create receiver thread\n");
    }
    pthread_t sender_thread;
    if(pthread_create(&sender_thread, NULL, init_sender, &tdat)) {
        perror("unable to create sender thread\n");
    }

    readyReceive();
    readySend();

    pthread_join(receiver_thread, NULL);
    pthread_join(sender_thread, NULL);

    return EXIT_SUCCESS;
}