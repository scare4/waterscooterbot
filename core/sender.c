/*
    waterscooterbot
    Copyright (C) 2019  Madeline MARIN-PACHE

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "bot.h"

extern unsigned short recvRd, sendRd;

enum message_type {
    DEFAULT_TXT = 0,
    PONG
};

struct message_s{
    char* message;
    char* channel;
    int type;
    struct message_s* next;
};

struct message_queue_s {
    struct message_s* head;
    struct message_s* tail;
    time_t time_sent[MESSAGE_CAP];
};
struct message_queue_s message_queue;

int sendNext(int sock, FILE* logf) {
    time_t now = time(NULL);
    int i;
    char msg[SEND_BUF_SIZE];
    for(i = 0; i < MESSAGE_CAP; i++) {
        if(now - message_queue.time_sent[i] > MESSAGE_CAP_DELAY) {
            if(message_queue.head == NULL) {
                return 2;
            }
            message_queue.time_sent[i] = now;
            struct message_s* message = message_queue.head;
            message_queue.head = message->next;
            memset(msg, 0, SEND_BUF_SIZE * sizeof(char));
            int s;
            switch(message->type) {
                case PONG:
                    s = sprintf(msg, "PONG :tmi.twitch.tv\r\n");
                    break;

                default:
                    s = sprintf(msg, "PRIVMSG %s :%s\r\n", message->channel, message->message);
                    break;
            }
            
            if(s > SEND_BUF_SIZE) {
                fprintf(stderr, "message too long to be sent\n");
                free(message);
                return -1;
            }
            s = send(sock, msg, s, 0);
            time_t ltime;
            ltime=time(NULL);
            printlog(logf ,"[%.24s][send] %-3d bytes: %8s", ctime(&ltime), s, msg);
            return 0;
        }
    }
    return 1;
}

int pong(lua_State* L) {
    struct message_s* message = calloc(1, sizeof(struct message_s));
    message->type = PONG;
    if(message_queue.tail)
        message_queue.tail->next = message;
    message_queue.tail = message;
    if(message_queue.head == NULL)
        message_queue.head = message;
    return 0;
}

int queueMessage(lua_State* L) {
    int argc = lua_gettop(L);

    //check argument type
    int t = lua_type(L, argc);
    if(t != LUA_TSTRING) {
        return -1;
    }
    t = lua_type(L, argc - 1);
    if(t != LUA_TSTRING) {
        return -1;
    }

    struct message_s* message = calloc(1, sizeof(struct message_s));
    message->message = calloc(MAX_MESSAGE_LENGTH, sizeof(char));
    message->channel = calloc(MAX_CHANNEL_LENGTH, sizeof(char));


    //convert to c strings
    const char* str,* str2;
    str = lua_tolstring(L, argc - 1, NULL);
    strcpy(message->channel, str);
    str2 = lua_tolstring(L, argc, NULL);
    strcpy(message->message, str2);
    //printf("queued %s :%s\n", str2, str);

    //relink queue
    message->next = NULL;
    if(message_queue.tail)
        message_queue.tail->next = message;
    message_queue.tail = message;
    if(message_queue.head == NULL)
        message_queue.head = message;
    return 0;
}

void sender(int sock, FILE* logf) {
    while(1) {
        if(sendNext(sock, logf)) {
            __sleep(1);
        }
        //__sleep(1);
    }
}
void* init_sender(void* tdat) {
    memset(&message_queue, 0, sizeof(struct message_queue_s));
    sender(((threadData*)tdat)->sfd, ((threadData*)tdat)->logFile);
    return NULL;
}