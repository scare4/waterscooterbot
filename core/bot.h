/*
    waterscooterbot
    Copyright (C) 2019  Madeline MARIN-PACHE

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __BOT_CORE_H__
#define __BOT_CORE_H__

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <linux/tcp.h>
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

#define REC_BUF_SIZE 576
#define MAX_MESSAGE_LENGTH 500
#define MAX_CHANNEL_LENGTH 32
#define SEND_BUF_SIZE MAX_MESSAGE_LENGTH + MAX_CHANNEL_LENGTH + 32

#define MESSAGE_CAP 19
#define MESSAGE_CAP_DELAY 31

#define NICK "waterscooterbot"
#ifdef __DEBUG
#define CHANNEL "#scarelol444"
#else
#define CHANNEL "#voxf0x"
#endif

#include "token.h"
#ifndef TOKEN
#error <No token found>
#endif

typedef struct {
    int sfd;
    FILE* logFile;
}threadData;

/**
 * toggles the ready to receive status
 */
void readyReceive();

/**
 * toggles the ready to send status
 */
void readySend();

/**
 * thread safe sleep, takes seconds
 */
void __sleep(unsigned int n);

/**
 * thread safe sleep, takes nanoseconds
 */
void __sleepn(unsigned int n);

/**
 * lua function to queue a message to be sent
 */
int queueMessage(lua_State* L);

/**
 * sends a pong message to twitch
 */
int pong(lua_State* L);

/**
 * entry point for receiver thread, is passed a pointer to the socket's file descriptor
 */
void* init_receiver(void* sock);

/**
 * entry point for sender thread, is passed a pointer to the socket's file descriptor
 */
void* init_sender(void* sock);

/**
 * logs both to stdout and a file
 */
void printlog(FILE* f, char const* fmt, ...);

#endif