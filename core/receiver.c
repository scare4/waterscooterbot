/*
    waterscooterbot
    Copyright (C) 2019  Madeline MARIN-PACHE

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "bot.h"

extern unsigned short recvRd, sendRd;
lua_State *L;

static int sleep_l(lua_State* L) {
    int n = lua_gettop(L);
    int s = (int)lua_tonumber(L, n);
    __sleep(s);
    return 1;
}

void* init_receiver(void* tdat) {
    int sock = ((threadData*)tdat)->sfd;
    FILE* logf = ((threadData*)tdat)->logFile;
    printf("listenning for awnser\n");

    L = luaL_newstate();
    luaL_openlibs(L);
    lua_pushcfunction(L, sleep_l);
    lua_setglobal(L, "sleep");
    lua_pushcfunction(L, queueMessage);
    lua_setglobal(L, "__queueMessage");
    lua_pushcfunction(L, pong);
    lua_setglobal(L, "pong");

    if(luaL_dofile(L, "bot.lua")) {
        printf("something went wrong\n");
        int c = lua_gettop(L);
        const char* cc;
        cc = lua_tolstring(L, c, NULL);
        fprintf(stderr, "%s\n", cc);
        exit(EXIT_FAILURE);
    }

    char buf[REC_BUF_SIZE];
    while(1){
        int nread = recv(sock, buf, REC_BUF_SIZE, 0);
        if(errno == EAGAIN || errno == EWOULDBLOCK) {
            printf("skip\n");
            continue;
        }
        if(nread == -1) {
            perror("read");
            exit(EXIT_FAILURE);
        }
        if(!recvRd) {
            __sleep(1);
            continue;
        }

        time_t ltime;
        ltime=time(NULL);
        printlog(logf, "[%.24s][recv] %-3d bytes: %s", ctime(&ltime), nread, buf);
        lua_getglobal(L, "parseMsg");
        lua_gettop(L);
        lua_pushstring(L, buf);
        lua_call(L, 1, 0);
    }
    lua_close(L);
    return NULL;
}
