function owo(args, user, channel)
    if args[2] == nil then
        return
    end

    function merge(words, start)
        res = ""
        while words[start] do
            res = res..words[start].." "
            start = start + 1
        end
        return res
    end

    local phrase = ""
    if args[2]:sub(1,1) == "!" then
        phrase = "Nice try, you're on the naughty list now."
    else
        phrase = merge(args, 2)
    end
    phrase = phrase:gsub("[rl]", "w")
    phrase = phrase:gsub("[RL]", "W")
    phrase = phrase:gsub("n([aeiou])", "ny%1")
    phrase = phrase:gsub("N([aeiou])", "Ny%1")
    phrase = phrase:gsub("N([AEIOU])", "NY%1")
    phrase = phrase:gsub("n([AEIOU])", "nY%1")
    phrase = phrase:gsub("thought", "fought")
    phrase = phrase:gsub("th(%a+)", "d%1")
    phrase = phrase:gsub("ove", "uv")

    bot.chat(phrase, channel)
end
return owo