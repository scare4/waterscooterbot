function time(args, user, channel)
    function convertHour(hour, min)
        local suffix = "AM"
        if hour >= 12 then
            suffix = "PM"
        end

        if hour > 12 then
            hour = hour - 12
        elseif hour <= 0 then
            hour = hour + 12
        end

        if min < 10 then
            min = "0"..min
        end
        return hour, suffix, min
    end

    local t = os.date("*t")
    local h, s, m = convertHour(t.hour - 7, t.min)

    bot.chat("For voxf0x, it is currently "..h..":"..m.." "..s, channel)
end
return time