function last(args, user, channel)
    if not args[2] then
        bot.chat("please specify the link of the song", channel)
        return
    end
    if bot.harddata.waterscooter[args[2]] then
        local t = bot.formulateSec(os.time() - bot.harddata.waterscooter[args[2]].time)
        local m = "this song was last requested "
        if t.hours > 0 then
            m = m..tostring(t.hours).." hours "
        end
        if t.minutes > 0 or t.hours > 0 then
            m = m..tostring(t.minutes).." minutes "
        end
        m = m..tostring(t.seconds).." seconds ago by "..bot.harddata.waterscooter[args[2]].req
        bot.chat(m, channel)
    else
        bot.chat("this song has not been requested yet", channel)
    end
    --bot.chat("waterscooter was last requested long ago.", channel)
end
return last