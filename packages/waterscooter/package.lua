local pack = {
    id = "waterscooter",
    desc = "the best music only",
    commands = {
        drink = "drink",
        last = "last",
        sr = "sr",
        offshore = "offshore",
        dc = "disconnect",
        queue = "queue",
        dutch = "dutch",
        love = "love",
        owo = "owo",
        voxtime = "time",
        join = "join",
        bot = "bot",
        birthday = "birth"
    },
    perm = {
        offshore = 1,
        dc = 50
    }
}
function pack.init()
    if bot.data.waterscooter == nil then
        bot.data.waterscooter = {}
    end
    if bot.harddata.waterscooter == nil then
        bot.harddata.waterscooter = {}
    end
end

return pack
