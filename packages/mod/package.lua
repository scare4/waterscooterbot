local p = {
    id = "mod",
    desc = "moderation",
    customFilters = {},
    commands = {
        checkbot = "checkbot",
        clearbot = "clearbot",
        confirmbot = "confirmbot",
        deletebot = "deletebot"
    },
    perm = 100,
    init = function ()
        if bot.harddata.mod == nil then
            bot.harddata.mod = {followbot = {}}
        end
    end
}
p.customFilters["Followinbot%.com"] = "followbot"
--p.customFilters["boop"] = "followbot"
return p