return function (args, user, channel, message)
    if args[2] == nil then
        bot.chat("@"..user.." please specify the username you want to confirm as a normal user.", channel)
        return
    end
    if bot.harddata.mod.followbot[args[2]] then
        bot.harddata.mod.followbot[args[2]].state = "clear"
    else
        bot.harddata.mod.followbot[args[2]] = {
            state = "clear",
            flags = 1,
            lastflag = os.time()
        }
    end
    bot.chat("@"..user.." confirmed "..args[2].." as a normal user.", channel)
end