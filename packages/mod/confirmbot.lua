return function (args, user, channel, message)
    if args[2] == nil then
        bot.chat("@"..user.." please specify the username you want to confirm as a spam bot.", channel)
        return
    end
    if bot.harddata.mod.followbot[args[2]] then
        bot.harddata.mod.followbot[args[2]].state = "bot"
    else
        bot.harddata.mod.followbot[args[2]] = {
            state = "bot",
            flags = 1,
            lastflag = os.time()
        }
    end
    bot.chat("@"..user.." confirmed "..args[2].." as a spam bot.", channel)
end
