return function (args, user, channel, message)

    if args[2] == nil then
        bot.chat("@"..user.." please specify the username you want to check.", channel)
        return
    end
    if bot.harddata.mod.followbot[args[2]] then
        if bot.harddata.mod.followbot[args[2]].state == "clear" then
            bot.chat(args[2].." has been confirmed as a normal user.", channel)
        elseif bot.harddata.mod.followbot[args[2]].state == "bot" then
            bot.chat(args[2].." has been confirmed as a bot. They have been flagged "..bot.harddata.mod.followbot[args[2]].flags.." times, the last time was on the "..os.date("%m/%d/%Y, %I:%M %p.", bot.harddata.mod.followbot[args[2]].lastflag - 18000), channel)
        else
            bot.chat(args[2].." has been flagged "..bot.harddata.mod.followbot[args[2]].flags.." times, the last time was on the "..os.date("%m/%d/%Y, %I:%M %p.", bot.harddata.mod.followbot[args[2]].lastflag - 25200), channel)
        end
    else
        bot.chat("I've never heard of "..args[2].." before.", channel)
    end
end