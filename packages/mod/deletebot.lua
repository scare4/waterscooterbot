return function (args, user, channel, message)
    if args[2] == nil then
        bot.chat("@"..user.." please specify the username you want to delete.", channel)
        return
    end
    bot.harddata.mod.followbot[args[2]] = nil
    bot.chat("@"..user..", "..args[2].." has been deleted.", channel)
end