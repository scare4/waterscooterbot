function unload(args, user, channel)
    if not args[2] then
        bot.chat("@"..user.." please specify the id of the package to unload", channel)
        return
    end
    bot.packloc = args[2]
    if pcall(bot.unloadPackage) then
        bot.chat("@"..user.." successfully unloaded package", channel)
    else
        bot.chat("@"..user.." failed to unload package", channel)
    end
    bot.packloc = nil
end
return unload