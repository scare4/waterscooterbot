return {
    id = "Package_management_tools",
    desc = "Tools to load/reload/unload packages at runtime",
    commands = {
        pkload = "pkload",
        pkunload = "pkunload",
        pkreload = "pkreload"
        --pklist = "pklist"
    },
    perm = 50
}