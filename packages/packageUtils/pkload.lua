function load(args, user, channel)
    if not args[2] then
        bot.chat("@"..user.." please specify the location of the package to load (package has to be in the packages folder)", channel)
        return
    end
    bot.packloc = args[2]
    if pcall(bot.loadPackage) then
        bot.chat("@"..user.." successfully loaded package", channel)
    else
        bot.chat("@"..user.." failed to load package", channel)
    end
    bot.packloc = nil
end
return load