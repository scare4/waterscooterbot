function reload(args, user, channel)
    if not args[2] then
        bot.chat("@"..user.." please specify the id of the package to reload", channel)
        return
    end
    if bot.packages[args[2]] == nil then
        bot.chat("@"..user.." this package is not loaded", channel)
        return
    end
    bot.packloc = args[2]
    bot.reloadPackage()
    if pcall(bot.reloadPackage) then
        bot.chat("@"..user.." successfully reloaded package", channel)
    else
        bot.chat("@"..user.." failed to reload package", channel)
    end
    bot.packloc = nil
end
return reload