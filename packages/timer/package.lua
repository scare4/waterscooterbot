local pack = {
    id = "timer",
    desc = "some timing util",
    commands = {
        timer = "timer"
    }
}
function pack.init()
    if bot.data.timer == nil then
        bot.data.timer = {}
    end
end

return pack