function timer(args, user, channel)
    if not bot.data.timer[user] or args[2] then
        if not bot.data.timer[user] or args[2] == "set" then
            bot.data.timer[user] = os.time()
            bot.chat("@"..user.." timer set", channel)
            return
        end
    else
        local t = bot.formulateSec(os.time() - bot.data.timer[user])
        local m = "@"..user.." "
        if t.hours > 0 then
            m = m..tostring(t.hours).." hours "
        end
        if t.minutes > 0 or t.hours > 0 then
            m = m..tostring(t.minutes).." minutes "
        end
        m = m..tostring(t.seconds).." seconds have passed"
        bot.chat(m, channel)
    end
end
return timer