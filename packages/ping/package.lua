return {
    id = "ping",
    desc = "adds a ping command",
    commands = {
        ping = "ping",
        pong = "pong"
    }
}